package ex31;

public class AmountNotSufficientException extends Exception {
    public AmountNotSufficientException(String s) {
        super(s);
    }
}
