package ex31;

public class Account {

    private float amount;

    public Account(float amount) {
        this.amount = amount;
    }

    public float getAmount() {
        return amount;
    }

    public void addCredit(float i) throws AmountNegativeException {
        if (i < 0)
            throw new AmountNegativeException("Amount given is negative, and not accepted");
        amount += i;
    }

    public void transferTo(Account b, float v) throws AmountNegativeException, AmountNotSufficientException {

        b.addCredit(v);

        if(amount < v)
            throw new AmountNotSufficientException("Amount to transfer is too much");
        amount -= v;
    }
}
