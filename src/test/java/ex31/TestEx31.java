package ex31;

import org.junit.*;

import static org.junit.Assert.*;

public class TestEx31 {

    Account a;
    @Before
    public void InitTest()
    {
        a = new Account(10);
    }


    @Test
    public void AccountInitTest()
    {
        assertEquals(10.0f, a.getAmount(), 0);
    }

    @Test
    public void AccountCreditTest() throws AmountNegativeException {
        a.addCredit(5);

        assertEquals(15.0f, a.getAmount(), 0);
    }

    @Test
    public void AccountDebitTest() throws AmountNegativeException {
        a.addCredit(5);

        assertEquals(15.0f, a.getAmount(), 0);
    }

    @Test
    public void AccountTransferTest() throws AmountNotSufficientException, AmountNegativeException {
        Account b = new Account(8.0f);
        b.addCredit(1.0f);

        a.transferTo(b, 10.0f);

        assertEquals(0.0f, a.getAmount(), 0.0f);
    }
}
