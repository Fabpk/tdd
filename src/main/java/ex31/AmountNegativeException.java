package ex31;

public class AmountNegativeException extends Exception {
    public AmountNegativeException(String s) {
        super(s);
    }
}
